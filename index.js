/**
 *
 * @param {WeakMap} weakmap
 */
const createPrivateObjectWithWeakMap = weakMap => instance => {
  let privateObject = {};
  let weakMapValues = weakMap.get(instance);

  Object.keys(weakMapValues).forEach(weakMapKey =>
    Object.defineProperty(privateObject, weakMapKey, {
      writable: true,
      get: () => weakMapValues[weakMapKey],
      set: value =>
        weakMap.set(instance, { ...weakMapValues, [weakMapKey]: value })
    })
  );

  return privateObject;
};

/**
 * Creates a new class with the given attributes
 *
 * @param {object} param
 * @param {string|symbol} param.name identifier for the class
 * @param {method[]} [param.methods=[]] functions attached to the class
 * @param {field[]} [param.fields=[]]
 * data properties attached to each instance of the class
 *
 * @return {function} the created class
 */
const create = ({ name, methods = [], fields = [] } = {}) => {
  const privateFields = fields.filter(field => field.private);
  const publicFields = fields.filter(field => !field.private);

  let createdClass = class {};
  let privateMap = new WeakMap();
  let createPrivateObject = createPrivateObjectWithWeakMap(privateMap);

  Object.defineProperty(createdClass, 'name', { value: name });

  methods.forEach(method => {
    Object.defineProperty(createdClass, method.name, {
      enumerable: true,
      value: method.func
    });
  });

  Object.defineProperty(createdClass, 'constructor', {
    value: function() {
      /*
        Setup the private fields in the WeakMap
        for the in-construction object
      */
      privateMap.set(
        this,
        privateFields.reduce((resultObject, privateField) => {
          resultObject[privateField.name] = undefined;
        }, {})
      );

      /*
        Need to create the privateObject
        after the privateMap has its keys set
      */
      let privateObject = createPrivateObject(this);

      Object.getOwnPropertyNames(this.__proto__)
        .filter(property => typeof this[property] === 'function')
        .forEach(
          functionProperty =>
            (this[functionProperty] = this[functionProperty].bind(
              this,
              privateObject
            ))
        );
    }
  });

  /*
    Setup the public fields on the class,
    they can be modified afterwards, no restriction
  */
  publicFields.forEach(publicField =>
    Object.defineProperty(createdClass.prototype, publicField.name, {
      enumerable: true,
      configurable: true,
      writable: true
    })
  );

  return createdClass;
};

exports.create = create;

/**
 * Class function, this function should not be an arrow function
 * if 'this' is used inside of it.
 *
 * @typedef {object} method
 *
 * use 'this' to access the calling object.
 *
 * @property {string|symbol} name
 * @property {function} func
 * the actual function called when calling the method
 */

/**
 * Class data property
 *
 * @typedef {object} field
 *
 * @property {string|symbol} name
 * @property {boolean} private whether the field is private
 */
