const { create } = require('../index');

/*
  Tests name format:

  [top-level function]
    feature: a feature of this function
      a contract of this feature
        (conditions for this contract to be true)
*/

describe('[create function]', () => {
  describe('feature: name the class,', () => {
    /**
     * @type {string|symbol}
     */
    let myName;
    /**
     * @type {function}
     */
    let myClass;
    /**
     * @type {object}
     */
    let myObject;

    describe('with a string', () => {
      beforeEach(() => {
        myName = 'myClass';
        myClass = create({ name: myName });
        myObject = new myClass();
      });

      it('(name of the class must match expected)', () =>
        expect(myClass.name).toBe(myName));

      it('(object constructor name must match expected)', () =>
        expect(myObject.constructor.name).toBe(myName));

      it('(object instance must match expected)', () =>
        expect(myObject instanceof myClass));
    });

    describe('with a symbol', () => {
      beforeEach(() => {
        myName = Symbol('myClass');
        myClass = create({ name: myName });
        myObject = new myClass();
      });

      it('(name of the class must match expected)', () =>
        expect(myClass.name).toBe(myName));

      it('(object constructor name must match expected)', () =>
        expect(myObject.constructor.name).toBe(myName));

      it('(object instance must match expected)', () =>
        expect(myObject instanceof myClass));
    });
  });

  describe('feature: define fields for the class,', () => {
    /**
     * @type {string}
     */
    let myName;
    /**
     * @type {function}
     */
    let myClass;
    /**
     * @type {object}
     */
    let myObjectA;
    /**
     * @type {object}
     */
    let myObjectB;
    /**
     * @type {string}
     */
    let myFieldNameX;
    /**
     * @type {string}
     */
    let myFieldNameY;
    /**
     * @type {string}
     */
    let notMyFieldName;

    describe('field is part of the prototype', () => {
      beforeEach(() => {
        myName = 'myClass';
        myFieldNameX = 'fieldX';
        notMyFieldName = 'notField';
        myClass = create({
          name: myName,
          fields: [{ name: myFieldNameX, private: false }]
        });
        myObjectA = new myClass();
      });

      it('(defined field must be in object prototype)', () =>
        expect(myFieldNameX in myObjectA).toBeTruthy());

      it('(undefined field must not be in object prototype)', () =>
        expect(notMyFieldName in myObjectA).not.toBeTruthy());
    });

    describe('field is public by default', () => {
      beforeEach(() => {
        myName = 'myClass';
        myFieldNameX = 'fieldX';
        myClass = create({
          name: myName,
          fields: [{ name: myFieldNameX }]
        });
        myObjectA = new myClass();
      });

      it('(public field must be in object prototype)', () =>
        expect(myFieldNameX in myObjectA).toBeTruthy());
    });

    describe('fields are isolated', () => {
      beforeEach(() => {
        myName = 'myClass';
        myFieldNameX = 'fieldX';
        myClass = create({
          name: myName,
          fields: [{ name: myFieldNameX, private: false }]
        });
        myObjectA = new myClass();
        myObjectB = new myClass();
      });

      it('(values of a field are not shared between instances)', () => {
        myObjectA[myFieldNameX] = 123;
        myObjectB[myFieldNameX] = 'Hello World';

        expect(myObjectA[myFieldNameX]).not.toBe(myObjectB[myFieldNameX]);
      });
    });

    describe('private fields cannot be accessed from outside', () => {
      beforeEach(() => {
        myName = 'myClass';
        myFieldNameX = 'fieldX';
        myClass = create({
          name: myName,
          fields: [{ name: myFieldNameX, private: true }]
        });
        myObjectA = new myClass();
      });

      it('(private field must not be in object prototype)', () =>
        expect(myFieldNameX in myObjectA).not.toBeTruthy());
    });
  });
});
